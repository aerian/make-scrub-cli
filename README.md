# Install

- Note: Installation assumes you have current versions of nodejs, NPM and GIT installed.

- Install FFMPEG
    - [Os X](https://trac.ffmpeg.org/wiki/MacOSXCompilationGuide)
    - [Linux](https://github.com/joshfng/install_ffmpeg)

- Install ImageMagick
    - On OsX you can use `brew install imagemagick`

- Clone Repo
    - `git clone https://bitbucket.com/aerian/make-scrub-cli && cd make-scrub-cli`

- Install dependancies
    - `npm install`

- Link Package
    - `npm link`

# Commands

## Make Scrub From Video
`make-scrub path/to/video.mpg path/to/output/folder`

  - Options to produce content and config will follow image processing
  - output folder must be different from path containing video.

## Make Scrub From Image Set
`make-scrub -c path/to/images/`

Produces a config.json document for a set of images. Assumes images are numerically sequenced with a filename as a prefix:

E.g
  - `sequence_001.jpg ,sequence_002.jpg` 

## Add Content or Edit Config
`make-scrub -e path/to/config.json`

This will allow you to add new content blocks or edit the config settings.

## Add Poster to Scrub
`make-scrub -t path/to/image.jpg -p path/to/config/`

This must be done after create an initial scrubbable object (set of images and a config.json). 
This gives you the chance to add a frame from video if you wish.
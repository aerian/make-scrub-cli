/*
 * process-scrub
 * ffmpeg -i "videos/test.mov" -q:v 1 "outputdir/%03d.jpg"
 *
 * Copyright (c) 2014 John Farrow
 * Licensed under the MIT license.
 */
'use strict';
var exec = require('child_process').exec;

function split(video) {
    var process = exec('ffmpeg -i "' + video + '" -q:v 1 "outputdir/%03d.jpg"',
        function(error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}

split('videos/test.mov');

exports.split = function() {

};
#!/usr/bin/env node

var split = require('./split.js').split,
  make_json = require('./add_files_to_config_obj.js').make_json,
  sizing = require('./process_image_size.js'),
  addHTML = require('./addHTML.js').addHTML,
  addConfig = require('./addConfig.js').addConfig,
  fs = require('fs'),
  nodePath = require('path'),
  ProgressBar = require('progress'),
  argv = require('minimist')(process.argv.slice(2)),
  config = {},
  path,
  video,
  edit,
  videoIsFirst,
  args,
  thumb,
  log = function() {};


video = argv.v || argv.video || argv._[0] || null;
path = argv.p || argv.path || argv._[1] || null;
edit = argv.e || argv.edit || null;
convertPath = argv.c || argv.convertpath || null;
thumb = argv.t || argv.thumbnail || null;

if (argv.d) {
  log = console.log;
}

function make_scrub(video, path) {
  log('Preparing Video For Scrubbable...');
  split(video, path, filesToConfig);
}

function filesToConfig() {
  log('Initialising configuration settings...');
  make_json(path, responsifyImages);
}

function responsifyImages(filesObj, files) {

  var i = 0, bar;
  log('Generating responsive images...');
  log(files[i]);
  log(files[i].match('.jpg'));
  function responsifyImageLoop() {
    i++;
    bar.tick();
    if (i < files.length) {
      processFile(files[i]);
    } else {
      addHTML(addContent);
    }
  }
  config.files = filesObj;

  bar = new ProgressBar('Processing Images [:bar] :percent :etas', { width: 50, total: files.length - 1 });

  function processFile(file) {

    if (file.match('.jpg')) {
      log('processing ' + file);
      sizing.process(file, path, responsifyImageLoop);
    } else {
      log('skipping ' + file);
      responsifyImageLoop();
    }
  };
  processFile(files[i]);
}

function openConfig(path) {
  log('Opening Config...');

  fs.readFile(path, 'utf8', function(err, data) {
  if (err) throw err;
    config = getConfig(path);
    addHTML(addContent);
  });
}

function getConfig(path) {
  return require(fs.realpathSync(path)).data;
}
function addContent(html) {
    log('Preparing To Update Content...');
    config.content = config.content && config.content.length ? config.content.concat(html) : html;
    addConfig(updateConfAndWrite);
}

function updateConfAndWrite(conf) {
  log('Preparing Content For Disk...');
  config.config = conf;
  writeFile();
}

function addThumb(img, path) {
  console.log('Adding Poster');

  /*
  img: img to turn into thumb
  path: to config area. Assumes config has been 
   */
  var dir = nodePath.join(path, 'poster/'),
      configPath = nodePath.join(path, 'config.js'),
      config = getConfig(configPath),
      imageName = nodePath.basename(img);

  fs.mkdirSync(dir);
  sizing.split(img, dir, false,function() {
    config.poster = imageName;
    console.log('Saving');
    fs.open(configPath, 'w+', function(err, fd) {
      if (err) {
          console.error('Error opening file:' + err);
          console.trace();
      } else {

          
          fs.writeSync(fd, prepareDataForFile(config));
          console.log('Poster added!!');
      }
    });
  });
}

function prepareDataForFile(data){
  return "var aerian_scrubbable_data = " + JSON.stringify(data, null, 4) + "; var exports = exports || {};exports.data = aerian_scrubbable_data;";
}

function writeFile(file) {
  log('Writing Content To Disk...');
  var fileContent,
      filePath = file || edit || path + '/config.js';
  fs.open(filePath, 'w+', function(err, fd) {
      if (err) {
          console.error('Error opening file:' + err);
      } else {
          fs.writeSync(fd, prepareDataForFile(config));
          log('Done!!');
      }

  });
}

if (video && path) {
  make_scrub(video, path);
}

if (convertPath) {
  path = convertPath;
  filesToConfig();
}

if (edit) {
  openConfig(edit);
}

if (thumb) {
  console.log('Thumb path detected');
  addThumb(thumb, path);
}

exports.make = split;//exports for modular use

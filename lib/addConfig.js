var promptly = require('promptly'),
    generateAllSizes = require('./process_image_size.js').process,
    exit, config = {};

function addConfig(callback) {
    exit = exit || callback || function() {};
    promptly.prompt('Would you like to add config? Y/N', function(err, value) {
        if (!value.match(/[nN]/)) {
            setTitle();
        } else {
            exit(config);
        }
    });
}
function setTitle() {
    promptly.prompt('Set Title:', function(err, value) {
        if (typeof value !== 'string') {
            setTitle();
        } else {
            config.title = value;
            setDescription();
        }
    });
}
function setDescription() {
    promptly.prompt('Set Description:', function(err, value) {
        if (typeof value !== 'string') {
            setDescription();
        } else {
            config.description = value;
            setAxis();
        }
    });
}
function setAxis() {
    promptly.prompt('Set Axis (x or y)', function(err, value) {
        if (!value.match(/[xyXY]/)) {
            console.log('Must be X or Y');
            setAxis();
            return;
        }

        config.axis = value;
        setStartEdge();
    });
}
function setStartEdge() {
    var edgeSelect = config.axis === 'x' ? '(L)eft or (R)ight' : '(T)op or (B)ottom';
    promptly.prompt('Set start edge: ' + edgeSelect, function(err, value) {
        if (!value.match(/[lrtbLRTB]/)) {
            console.log('Must be an initial');
            setStartEdge();
            return;
        }

        config.edge = value;
        setOffset();
    });
}

function setOffset() {
    promptly.prompt('Set Edge Offset (must be percentage):', function(err, value) {
        var offset = Number(value);
        if (isNaN(offset) || offset < 0 || offset > 100) {
            console.log('Value must a number between 0 and 100');
            setOffset();
            return;
        }
        config.offset = offset;
        startSetFramerate();
    });
}

function startSetFramerate() {
    var validator = function(value) {
        if (!value.match(/[yYnN]/)) {
            throw new Error('Must Select Y or N');
        }

        return value;
    };
    promptly.prompt('Would you like manually set framerate of Scrub? Y/N', { validator: validator}, function(err, value) {
        if (err) {
            console.error(e.message);
            return err.retry();
        }

        if (value.match(/[yY]/)) {
            setFramerate();
        } else {
            setMainColour();
        }
    });
};


function setFramerate() {
    var startMod = 1,
        devices = ['desktop', 'tablet', 'mobile'],
        validator = function(value) {
        value = Number(value);
        if (isNaN(value)) {
            throw new Error('Must be a number');
        }
        if (value < startMod) {
            throw new Error('Must be equal or greater to '+startMod)
        }
        startMod = value;
        return value;
    };

    config.deviceFramerates = {}

    function setDeviceFramerate() {
        var device = devices.splice(0, 1);
        promptly.prompt('Set the framerate for ' + device, {validator: validator}, function(err, value){
            if (err) {
                console.error(e.message);
                return err.retry();
            }

            config.deviceFramerates[device] = value;
            if (devices.length) {
                setDeviceFramerate();
            } else {
                setMainColour();
            }
        });

    };
    setDeviceFramerate();


};


function setMainColour() {
    promptly.prompt('Main Colour: (e.g #2EDFD2)', function(err, value) {
        if (value.length >= 1 && !value.match(/#[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3}$/)) {
            setMainColour();
        } else {
            config.color = {main: value};
            exit(config);
        }
    })
}





exports.addConfig = addConfig;

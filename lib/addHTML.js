var promptly = require('promptly'),
    content = [], newBlock, exit;

function chooseToAddNewBlock(callback) {
    exit = exit || callback || function() {};
    promptly.prompt('Would you like to add a new content block? Y/N', function(err, value) {
        if (!value.match(/n|N/)) {
            newBlock = {};
            setStartFrame();
        } else {
            exit(content);
        }
    });
}
function setStartFrame() {
    promptly.prompt('Set Start Frame:', function(err, value) {
        newBlock.startFrame = Number(value);
        setEndFrame();
    });
}
function setEndFrame() {
    promptly.prompt('Set End Frame:', function(err, value) {
        newBlock.endFrame = Number(value);
        setX();
    });
}
function setX() {
    promptly.prompt('Set X Offset (Percentage: 0-100):', function(err, value) {
        newBlock.x = Number(value);
        setY();
    });
}
function setY() {
    promptly.prompt('Set Y Offset (Percentage: 0-100):', function(err, value) {
        newBlock.y = Number(value);
        addHTML();
    });
}
function addHTML() {
    promptly.prompt('Add Content String:', function(err, value) {
        newBlock.html = value;
        content.push(newBlock);
        chooseToAddNewBlock();
    });
}


exports.addHTML = chooseToAddNewBlock;
